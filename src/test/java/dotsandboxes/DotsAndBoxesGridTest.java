package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    /*
     * Because Test classes are classes, they can have fields, and can have static fields.
     * This field is a logger. Loggers are like a more advanced println, for writing messages out to the console or a log file.
     */
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    /*
     * Tests are functions that have an @Test annotation before them.
     * The typical format of a test is that it contains some code that does something, and then one
     * or more assertions to check that a condition holds.
     *
     * This is a dummy test just to show that the test suite itself runs
     */
    @Test
    public void testTestSuiteRuns() {
        logger.info("Dummy test to show the test suite runs");
        assertTrue(true);
    }

    @Test
    public void boxCompleteTest() {
        DotsAndBoxesGrid A = new DotsAndBoxesGrid(4,3,1);
        A.drawHorizontal(0,0,1);
        A.drawHorizontal(1,0,1);
        A.drawVertical(0,0,1);
        A.drawVertical(0,1,1);
        asserTrue(A.boxComplete(0,0));
    }

    @Test
    public void drawHorizontalTest(){
        DotsAndBoxesGrid B = new DotsAndBoxesGrid(4,3,1);
        B.drawHorizontal(0,0,1);
        try{
          B.drawHorizontal(0,0,1);
        }
        catch (Exception e){
          assertEquals("Horizontal line already drawn", e.getMessage());
        }

    }

    @Test
    public void drawVerticalTest(){
        DotsAndBoxesGrid C = new DotsAndBoxesGrid(4,3,1);
        C.drawVertical(0,0,1);
        try{
          C.drawVertical(0,0,1);
        }
        catch (Exception e){
          assertEquals("Vertical line already drawn", e.getMessage());
        }

    }    // FIXME: You need to write tests for the two known bugs in the code.
}
